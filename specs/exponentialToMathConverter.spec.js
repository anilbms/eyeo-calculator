describe('Exponential to math converter class', function() {
	var exponentialToMathConverter = new ExponentialToMathConverter();

	it('getMathFormGivenString should return Math form for given string with exponential', function() {
		expect(exponentialToMathConverter.getMathFormGivenString('+e1')).toBe('+Math.pow(10, 1)');
		expect(exponentialToMathConverter.getMathFormGivenString('+e-1')).toBe('+Math.pow(10, -1)');
		expect(exponentialToMathConverter.getMathFormGivenString('+e-1*')).toBe('+Math.pow(10, -1)*');
		expect(exponentialToMathConverter.getMathFormGivenString('+e1*')).toBe('+Math.pow(10, 1)*');
		expect(exponentialToMathConverter.getMathFormGivenString('+23e1')).toBe('+23*Math.pow(10, 1)');
		expect(exponentialToMathConverter.getMathFormGivenString('+e1')).toBe('+Math.pow(10, 1)');
		expect(exponentialToMathConverter.getMathFormGivenString('2e-2')).toBe('2*Math.pow(10, -2)');
		expect(exponentialToMathConverter.getMathFormGivenString('e-2')).toBe('Math.pow(10, -2)');
		expect(exponentialToMathConverter.getMathFormGivenString('28+65/3e+1')).toBe('28+65/3*Math.pow(10, +1)');
		expect(exponentialToMathConverter.getMathFormGivenString('28+3e+1+33*e-2')).toBe('28+3*Math.pow(10, +1)+33*Math.pow(10, -2)');
	});

	it('getMathFormGivenString should return input string for given string with non-exponential', function() {
		expect(exponentialToMathConverter.getMathFormGivenString('abc')).toBe('abc');
		expect(exponentialToMathConverter.getMathFormGivenString('xyz')).toBe('xyz');
		expect(exponentialToMathConverter.getMathFormGivenString('+xedjsdks-*')).toBe('+xedjsdks-*');
	});
});