## Calculator

###Folder structure 
eyeo-domain-search
	src/js/
	specs/
	sketchs/
	style-guide/
	index.html
	style.css
	app.js
	exponentialToMathConverter.js

### Test application 
After cloning the repository, perform *npm update*.
Unit tests are written using [Jasmine](http://jasmine.github.io/).
To run test perform *npm test*
