/**
 * @constructor
 */
function ExponentialToMathConverter() {

}

ExponentialToMathConverter.prototype = {
    /**
     * check if given string  has any match to following pattern
     * operators(+-*\/)/numbers[0-9]/e/operators(+-)/numbers[0-9]/operators(+-) format
     * using regex
     * @private
     * @param  {string} exp
     * @return {boolean}
     */
    _IsExpressionHasExponential: function(exp) {
        return /[\+\-\*\/]*\d*[e]+[\+\-]*\d+[\+\-\*\/]*/g.test(exp);
    },
    /**
     * format given exponential expression into math equivalent form using regex
     * @private
     * @param  {string} exp
     * @return {string}
     */
    _getExponentialInMathOperation: function(exp) {
        var operations = exp.match(/[\+\-*\/]*/g),
            numbers = exp.match(/\d+/g),
            result = '';

        // operators(+-*\/)
        if (exp.match(/[\+\-\*\/]+[\d]*e/)) {
            result += exp.match(/[\+\-\*\/]+[\d]*e/)[0][0];
        }
        //numbers(0-9)
        if (exp.match(/[\+\-\*\/]*[\d]+e/)) {
            result += exp.match(/[\d]+/)[0] + '*';
        }

        result += 'Math.pow(10, ';
        //operators(+-)
        if (exp.match(/e[\+\-]+[\d]+/)) {
            result += exp.match(/e[\+\-]+[\d]+/)[0].match(/[\+\-]/)[0];
        }
        //numbers(0-9)
        if (exp.match(/e[\+\-]*[\d]+/)) {
            result += exp.match(/e[\+\-]*[\d]+/)[0].match(/[\d]+/)[0] + ')';
        }
        // operators(+-*\/)
        if (exp.match(/e[\+\-]*[\d]+[\+\-\*\/]+/)) {
            result += exp.match(/e[\+\-]*[\d]+[\+\-\*\/]+/)[0].match(/[\d]+[\+\-\*\/]+/)[0].match(/[\+\-\*\/]+/);
        }
        return result;
    },

    _getMathFormForEachExponential: function(exp) {
        var result = exp.replace(/[\+\-\*\/]*\d*[e]+[\+\-]*\d+[\+\-\*\/]*/g, this._getExponentialInMathOperation);

        return result;
    },
    /**
     * get math equivalent form for given exponential expression
     * @public
     * @param  {string} value
     * @return {string}
     */
    getMathFormGivenString: function(value) {
        if (this._IsExpressionHasExponential(value)) {
            return this._getMathFormForEachExponential(value);
        } else {
            return value;
        }
    }
};
